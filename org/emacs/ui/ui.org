* Интерфейс
:PROPERTIES:
:header-args:emacs-lisp: :results output silent  :tangle ~/.orgunix/emacs/init/10-ui.el
:END:
** Цвета

 После того, как тема загружена (after-load-theme-hook)

#+BEGIN_SRC emacs-lisp

(defvar after-load-theme-hook nil
"Hook run after a color theme is loaded using `load-theme'.")

(defadvice load-theme (after run-after-load-theme-hook activate)
"Run `after-load-theme-hook'."
(run-hooks 'after-load-theme-hook))

#+END_SRC

 Тема

#+BEGIN_SRC emacs-lisp 


(setq-default color-theme-is-cumulative nil)

(use-package tao-theme
:load-path "~/elisp/tao-theme-emacs"
:config 

(defun tao-palette ()
 (tao-theme-yang-palette))

(setq-default tao-theme-use-height nil)

(if (window-system) (progn
                     (require 'tao-yang-theme)
                     (load-theme 'tao-yang t)))


#+END_SRC  

 Отображение многоцветных тем на терминале       

#+BEGIN_SRC emacs-lisp
(use-package color-theme-approximate
:ensure t
:defer t
:config
(color-theme-approximate-on)))
#+END_SRC
** COMMENT Шрифты
#+BEGIN_SRC emacs-lisp
;; (when (window-system)
;;   (set-default-font "Fira Code"))
;; (let ((alist '((33 . ".\\(?:\\(?:==\\|!!\\)\\|[!=]\\)")
;;                (35 . ".\\(?:###\\|##\\|_(\\|[#(?[_{]\\)")
;;                (36 . ".\\(?:>\\)")
;;                (37 . ".\\(?:\\(?:%%\\)\\|%\\)")
;;                (38 . ".\\(?:\\(?:&&\\)\\|&\\)")
;;                (42 . ".\\(?:\\(?:\\*\\*/\\)\\|\\(?:\\*[*/]\\)\\|[*/>]\\)")
;;                (43 . ".\\(?:\\(?:\\+\\+\\)\\|[+>]\\)")
;;                (45 . ".\\(?:\\(?:-[>-]\\|<<\\|>>\\)\\|[<>}~-]\\)")
;;                (46 . ".\\(?:\\(?:\\.[.<]\\)\\|[.=-]\\)")
;;                (47 . ".\\(?:\\(?:\\*\\*\\|//\\|==\\)\\|[*/=>]\\)")
;;                (48 . ".\\(?:x[a-zA-Z]\\)")
;;                (58 . ".\\(?:::\\|[:=]\\)")
;;                (59 . ".\\(?:;;\\|;\\)")
;;                (60 . ".\\(?:\\(?:!--\\)\\|\\(?:~~\\|->\\|\\$>\\|\\*>\\|\\+>\\|--\\|<[<=-]\\|=[<=>]\\||>\\)\\|[*$+~/<=>|-]\\)")
;;                (61 . ".\\(?:\\(?:/=\\|:=\\|<<\\|=[=>]\\|>>\\)\\|[<=>~]\\)")
;;                (62 . ".\\(?:\\(?:=>\\|>[=>-]\\)\\|[=>-]\\)")
;;                (63 . ".\\(?:\\(\\?\\?\\)\\|[:=?]\\)")
;;                (91 . ".\\(?:]\\)")
;;                (92 . ".\\(?:\\(?:\\\\\\\\\\)\\|\\\\\\)")
;;                (94 . ".\\(?:=\\)")
;;                (119 . ".\\(?:ww\\)")
;;                (123 . ".\\(?:-\\)")
;;                (124 . ".\\(?:\\(?:|[=|]\\)\\|[=>|]\\)")
;;                (126 . ".\\(?:~>\\|~~\\|[>=@~-]\\)")
;;                )
;;              ))
;;   (dolist (char-regexp alist)
;;     (set-char-table-range composition-function-table (car char-regexp)
;;                           `([,(cdr char-regexp) 0 font-shape-gstring]))))
#+END_SRC
** Сокращения

  Сокращение диалогов yes/no  до y/n

#+BEGIN_SRC emacs-lisp 
(defalias 'yes-or-no-p 'y-or-n-p)
#+END_SRC

** Прокрутка

#+BEGIN_SRC emacs-lisp  
(setq-default scroll-conservatively 101
           scroll-step 0
           scroll-margin 5
           hscroll-step 0
           hscroll-margin 1)
#+END_SRC

** Увеличение

#+BEGIN_SRC emacs-lisp  
  (global-set-key (kbd "C-+") 'text-scale-increase)
  (global-set-key (kbd "C-=") 'text-scale-increase)
  (global-set-key (kbd "C-M-=") 'text-scale-set)
  (global-set-key (kbd "C--") 'text-scale-decrease)
#+END_SRC

** Перенос

   Невидимые части строки не переносятся, но в случае переноса, курсор перемещается по видимому вверх-вниз

   С-$ переключает режим переноса

#+BEGIN_SRC emacs-lisp 
  (setq-default truncate-lines t
             truncate-partial-width-windows 20
             longlines-show-hard-newlines t
             line-move-visual t)
  ;;(toggle-truncate-lines -1)
  (visual-line-mode t)
  (global-set-key (kbd "C-$") 'toggle-truncate-lines)
#+END_SRC

** Перемещение буферов

#+BEGIN_SRC emacs-lisp  

(use-package buffer-move
  :ensure t
  :defer t
  :config
  (global-set-key (kbd "s-K") 'buf-move-up)
  (global-set-key (kbd "s-J") 'buf-move-down)
  (global-set-key (kbd "s-H") 'buf-move-left)
  (global-set-key (kbd "s-L") 'buf-move-right))

#+END_SRC

** Cкрытые буферы

#+BEGIN_SRC emacs-lisp 
  (add-to-list 'display-buffer-alist
               (cons
                "\\*Async Shell Command\\*.*"
                (cons
                 #'display-buffer-no-window nil)))
#+END_SRC

** Золотое сечение

 Бывает удобно выделить несколько больше места текущему окну:
    
#+BEGIN_SRC emacs-lisp
(use-package golden-ratio
  :ensure t
  :defer t
  :bind(("C-x +" . golden-ratio)
        ("C-x =" . balance-windows)
        ("C-x _" . maximize-window)
        ("C-x -" . minimize-window))
  :init
  (golden-ratio-mode -1))
#+END_SRC

** Скриншоты

#+BEGIN_SRC emacs-lisp
(use-package screenshot
  :bind ("C-c s" . screenshot-take))
#+END_SRC
** Попапы

#+BEGIN_SRC emacs-lisp
(use-package popwin
:ensure t
:config
(popwin-mode 1)
(push '(:regexp :position top) popwin:special-display-config)
(setq-default popwin:special-display-config
              '(("*Miniedit Help*" :noselect t)
                help-mode
                (completion-list-mode :noselect t)
                (compilation-mode :noselect t)
                (grep-mode :noselect t)
                (occur-mode :noselect t)
                ("*Pp Macroexpand Output*" :noselect t)
                "*Shell Command Output*"
                "*vc-diff*"
                "*vc-change-log*"
                (" *undo-tree*" :width 60 :position right)
                ("^.*Developer.*$" :regexp t :width 80 :position right)
                ("^\\*anything.*\\*$" :regexp t)
                "*slime-apropos*"
                "*slime-macroexpansion*"
                "*slime-description*"
                ("*slime-compilation*" :noselect t)
                "*slime-xref*"
                (sldb-mode :stick t)
                slime-repl-mode slime-connection-list-mode))

)
#+END_SRC
