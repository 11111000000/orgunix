* Иконки
:PROPERTIES:
:header-args:emacs-lisp: :results output silent  :tangle ~/.orgunix/emacs/init/33-icons.el
:END:

Во первых нужны сами иконочные шрифты, поместим их в ~/.locale/share/fonts :

#+BEGIN_SRC sh :tangle no
  mkdir -p ~/.local/share/fonts/ ~/tmp/
  git clone git@github.com:domtronn/all-the-icons.el.git ~/tmp/all-the-icons
  cp ~/tmp/all-the-icons/fonts/* ~/.local/share/fonts
#+END_SRC

** all-the-icons

#+BEGIN_SRC emacs-lisp 

  (use-package all-the-icons
    :diminish "i"
    :ensure t
    :init 
    :config
    )

  (use-package all-the-icons-dired
    :diminish "di"
    :ensure t
    :init 
    :config
    (add-hook 'dired-mode-hook (lambda () (all-the-icons-dired-mode)))
    )
#+END_SRC
