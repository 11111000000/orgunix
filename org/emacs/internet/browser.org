* Браузер
:PROPERTIES:
:header-args:emacs-lisp: :results output silent  :tangle ~/.orgunix/emacs/init/29-browser.el
:END:

[[../../../static/meme.jpg]]

** Текстовый браузер

#+BEGIN_SRC emacs-lisp

(use-package w3m

  :ensure t


  :config

  (autoload 'w3m-browse-url "w3m" ">" t)

  (setq-default w3m-use-cookies t
                browse-url-new-window-flag t
                w3m-show-graphic-icons-in-header-line t
                w3m-display-inline-images t
                w3m-show-graphic-icons-in-mode-line t
                w3m-user-agent "Mozilla/5.0 (Linux; U; Android 2.3.3; zh-tw; HTC_Pyramid Build/GRI40) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533."
                w3m-session-load-always t
                w3m-session-autosave t
                w3m-session-load-last-sessions t)

  (defun w3m-view-this-url-background-session ()
    (interactive)
    (let ((in-background-state w3m-new-session-in-background))
      (setq w3m-new-session-in-background t)
      (w3m-view-this-url-new-session)
      (setq w3m-new-session-in-background in-background-state)))

  (defadvice browse-url-url-at-point (after w3m-anchor-at-point activate)
    "Browse the url at point. If w3m-anchor finds a url, use it."
    (setq ad-return-value 
          (or
           (w3m-anchor)
           ad-return-value)))

  (defun w3m-browse-url-other-window (url &optional newwin)
    (let ((w3m-pop-up-windows t))
      (if (one-window-p) (split-window))
      (other-window 1)
      (w3m-browse-url url newwin)))

  (defun w3m-copy-url-at-point ()
    (interactive)
    (let ((url (w3m-anchor)))
      (if (w3m-url-valid url)
          (kill-new (w3m-anchor))
        (message "No URL at point!"))))

  (defun w3m-open-current-page-in-chromium ()
    "Open the current URL in Mozilla Chromium."
    (interactive)
    (browse-url-chromium w3m-current-url))

  (defun w3m-open-link-or-image-in-chromium ()
    "Open the current link or image in Chromium."
    (interactive)
    (browse-url-chromium (or (w3m-anchor) 
                             (w3m-image))))

  (defun w3m-download-with-wget (loc)
    (interactive "DSave to: ")
    (let ((url (or (w3m-anchor) (w3m-image))))
      (if url
          (let ((proc (start-process "wget" (format "*wget %s*" url)
                                     "wget" "--passive-ftp" "-nv"
                                     "-P" (expand-file-name loc) url)))
            (with-current-buffer (process-buffer proc)
              (erase-buffer))
            (set-process-sentinel proc (lambda (proc str)
                                         (message "wget download done"))))
        (message "Nothing to get"))))

  (defun w3m-download-with-curl (loc)
    (define-key w3m-mode-map "c"
      (lambda (dir)
        (interactive "DSave to: ")
        (cd dir)
        (start-process "curl" "*curl*" "curl.exe" "-O" "-s" (w3m-anchor)))))

  ;;(require 'popwin-w3m)

  (add-hook 'w3m-mode-hook
            (lambda ()
              (w3m-lnum-mode 1)
              (local-unset-key (kbd "C-d"))
              (local-unset-key (kbd "d"))
              (define-keys w3m-mode-map
                (list
                 (kbd "M-n") 'w3m-tab-next-buffer
                 (kbd "M-p") 'w3m-tab-previous-buffer
                 (kbd "n") 'w3m-next-anchor
                 (kbd "p") 'w3m-previous-anchor
                 (kbd "C-<tab>") 'w3m-tab-next-buffer
                 (kbd "C-<iso-lefttab>") 'w3m-tab-previous-buffer
                 [C-S-tab] 'w3m-tab-previous-buffer
                 (kbd "M-o") 'w3m-goto-url-new-session
                 (kbd "O") 'w3m-goto-url-new-session
                 (kbd "o") 'w3m-goto-url
                 "d" nil
                 (kbd "C-d") nil
                 (kbd "C-c k") 'w3m-delete-buffer
                 (kbd "C-x k") 'w3m-delete-buffer
                 (kbd "q") 'w3m-close-window
                 (kbd "dd") 'w3m-delete-buffer
                 (kbd "B") 'w3m-view-previous-page
                 (kbd "F") 'w3m-view-next-page
                 (kbd "M-B") 'w3m-view-previous-page
                 (kbd "M-F") 'w3m-view-next-page
                 (kbd "b") 'w3m-select-buffer
                 (kbd "C-b") 'backward-char
                 (kbd "C-r") 'w3m-reload-this-page
                 (kbd "M-S-y") '(lambda () (interactive) (w3m-goto-url (x-get-clipboard)))
                 (kbd "M-<left>") 'w3m-view-previous-page
                 (kbd "M-<right>") 'w3m-view-next-page
                 (kbd "C-c y") '(lambda () (interactive) (kill-new w3m-current-url))
                 (kbd "C-c RET") 'w3m-open-current-page-in-chromium                 
                 (kbd "<M-return>") 'w3m-view-this-url-background-session
                 (kbd "<M-S-return>") 'browse-url-at-point
                 )
                )
              )
            )

  (add-hook 'w3m-lnum-mode-hook
            (lambda ()
              (define-keys w3m-lnum-mode-map (list
                                              (kbd "C-d") nil
                                              "d" nil
                                              (kbd "C-x k") 'w3m-delete-buffer
                                              (kbd "C-c k") 'w3m-delete-buffer
                                              (kbd "C-x C-k") 'w3m-delete-buffer
                                              (kbd "C-c C-k") 'w3m-delete-buffer
                                              (kbd "t") 'w3m-lnum-follow
                                              (kbd "C-t") 'w3m-lnum-follow
                                              (kbd "M-t") 'w3m-lnum-universal
                                              (kbd "C-M-t") 'w3m-lnum-universal)))))

#+END_SRC   

** Браузер по умолчанию 

 TODO: Заменить на surf или uzbl или другие, без вкладок. Вкладки - буферы EMACS

   #+BEGIN_SRC emacs-lisp 
      (setq-default browse-url-browser-function 'browse-url-chromium
                    browse-url-generic-program "chromium")

   #+END_SRC

** Отладчик Chromium

  Позволяет подключаться к консоли браузера Chrome, смотреть вывод, выполнять комманды

#+BEGIN_SRC emacs-lisp 
(use-package kite-mini
  :ensure t
  :defer t
  :bind (("C-c p j" . kite-mini-console))
  :init
  (require 'kite-mini-console)
  ;;(add-hook 'js-mode-hook (lambda () (kite-mini-mode t)))
  ;;(add-hook 'css-mode-hook (lambda () (kite-mini-mode t)))
)

#+END_SRC
** Поиск
*** engine mode

      #+BEGIN_SRC emacs-lisp  

       (use-package engine-mode 

         :ensure t
         :config

         (setq engine/browser-function 'w3m-goto-url-new-session)

         (engine/set-keymap-prefix (kbd "C-c e"))

         (defengine amazon
           "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=%s")

         (defengine aliexpress
           "https://ru.aliexpress.com/af/%s.html"
           :browser 'browse-url-generic
           :keybinding "ali")

         (defengine duckduckgo
           "https://duckduckgo.com/?q=%s"
           :keybinding "dg")

         (defengine github
           "https://github.com/search?ref=simplesearch&q=%s"
           :keybinding "gh")

         (defengine google
           "http://www.google.com/search?ie=utf-8&oe=utf-8&q=%s"
           :keybinding "go")

         (defengine google-images
           "http://www.google.com/images?hl=en&source=hp&biw=1440&bih=795&gbv=2&aq=f&aqi=&aql=&oq=&q=%s"
           :keybinding "gi")

         (defengine google-maps
           "http://maps.google.com/maps?q=%s"
           :keybinding "gm"    
           :docstring "Mappin' it up.")

         (defengine project-gutenberg
           "http://www.gutenberg.org/ebooks/search.html/?format=html&default_prefix=all&sort_order=&query=%s")

         (defengine rfcs
           "http://pretty-rfc.herokuapp.com/search?q=%s")

         (defengine so
           "https://stackoverflow.com/search?q=%s"
           :keybinding "so"    )

         (defengine ru-so
           "https://ru.stackoverflow.com/search?q=%s"
           :keybinding "rso"    )

         (defengine twitter
           "https://twitter.com/search?q=%s"
           :keybinding "tw"    )

         (defengine wikipedia
           "http://www.wikipedia.org/search-redirect.php?language=ru&go=Go&search=%s"
           :keybinding "wi"
           :docstring "Searchin' the wikis.")

         (defengine wiktionary
           "https://www.wikipedia.org/search-redirect.php?family=wiktionary&language=en&go=Go&search=%s")

         (defengine wolfram-alpha
           "http://www.wolframalpha.com/input/?i=%s")

         (defengine youtube
           "http://www.youtube.com/results?aq=f&oq=&search_query=%s"
           :keybinding "yo")

         (engine-mode))
      #+END_SRC

*** google-this

      #+BEGIN_SRC emacs-lisp 

       (use-package google-this
         :ensure t
         :defer t
         :init
         ;;(global-set-key (kbd "C-c C-g") 'google-this)
       )

      #+END_SRC

** Новости

     #+BEGIN_SRC emacs-lisp 

       (use-package elfeed
         :ensure t
         :defer t
         :config
         (define-keys elfeed-search-mode-map (list
                                              (kbd "C-j") 'next-line
                                              (kbd "C-k") 'previous-line
                                              (kbd "C-l") 'elfeed-search-show-entry
                                              (kbd "j") 'next-line
                                              (kbd "k") 'previous-line
                                              (kbd "l") 'elfeed-search-show-entry))
         (define-keys elfeed-show-mode-map (list
                                            (kbd "h") 'elfeed-kill-buffer ))  
         (setq elfeed-feeds

               '(("https://lenta.ru/rss" news russia)
                 ("https://actualidad.rt.com/feed" news russia)
                 ("http://static.feed.rbc.ru/rbc/logical/footer/news.rss" news)
                 ("https://meduza.io/rss/all" news russia)
                 ("http://newsbabr.com/rss/news.xml" news irkutsk)
                 ("https://www.irk.ru/news.rss" news irkutsk)
                 ("https://postnauka.ru/feed" science)
                 ("https://gorky.media/feed/" science)
                 ("http://news.rambler.ru/rss/world/" news world)
                 ("http://news.yandex.ru/index.rss" news world)
                 ("http://www.aif.ru/rss/all.php" news world russia)
                 ("https://www.vz.ru/rss.xml" news russia)
                 ("https://www.consultant.ru/rss/hotdocs.xml" news law russia)
                 ("https://www.consultant.ru/rss/fd.xml" law russia)
                 ("https://www.consultant.ru/rss/nw.xml" law russia)
                 ("http://tayga.info/all.rss" news russia irkutsk)
                )
               elfeed-search-filter "@2-days-ago +unread"))

     #+END_SRC

** Погода

   #+BEGIN_SRC emacs-lisp  

     (use-package wttrin
       :ensure t
       :defer t
       :commands (wttrin)
       :bind (("<f1> W" . wttrin))
       :init
       (setq wttrin-default-accept-language '("Accept-Language" . "ru-RU,ru")
             wttrin-default-cities '("Moscow"
                                     "Novosibirsk"
                                     "Krasnoyarsk"
                                     "Irkutsk"
                                     "Angarsk"
                                     "Voronezh"
                                     "Rossosh")))
   #+END_SRC

** Вопросы и Ответы

       #+BEGIN_SRC emacs-lisp  

         (use-package howdoi
           :ensure t
           :defer t
           :bind* (("C-c h d q" . howdoi-query)
                   ("C-c h d p" . howdoi-query-line-at-point)
                   ("C-c h d s" . howdoi-query-insert-code-snippet-at-point)))
       #+END_SRC

       #+BEGIN_SRC emacs-lisp  
         (use-package sx
           :defer t
           :ensure t)
       #+END_SRC    

** Карты

 Географические карты. Например, карта проприетарного Гугла:

     #+BEGIN_SRC emacs-lisp  

       (use-package google-maps
         :ensure t
         :defer t
         :init
         (require 'google-maps-static)
         (define-keys google-maps-static-mode-map (list
                                                   "h" 'google-maps-static-move-west
                                                   "j" 'google-maps-static-move-south
                                                   "k" 'google-maps-static-move-north
                                                   "l" 'google-maps-static-move-east
                                                   )))

     #+END_SRC

** Почта

    #+BEGIN_SRC emacs-lisp

      (add-to-list 'load-path "~/.nix-profile/share/emacs/site-lisp/mu4e")

      (require 'mu4e-contrib)

      (use-package mu4e
        :disabled t
        :defer t
        :init

        (define-keys mu4e-headers-mode-map
          (list
           (kbd "C-%") 'mu4e-headers-change-sorting
           "%" 'mu4e-headers-change-sorting
           "j" 'mu4e-headers-next
           "k" 'mu4e-headers-prev
           ))

        (define-keys mu4e-view-mode-map
          (list
           (kbd "C-n") 'mu4e-view-headers-next
           (kbd "C-p") 'mu4e-view-headers-prev
           ))

        (setq mu4e-html2text-command 'mu4e-shr2text)

        ;;(setq mu4e-html2text-command
        ;;     "html2text | grep -v '&nbsp_place_holder;'")

        ;;(setq mu4e-html2text-command "html2text -utf8 -width 72 -style pretty")

        (setq message-send-mail-function   'smtpmail-send-it
              smtpmail-default-smtp-server "smtp.mail.com"
              smtpmail-smtp-server         "smtp.mail.com"
              smtpmail-smtp-user           "11111000000@email.com"
              mu4e-view-prefer-html        nil
              mu4e-view-show-images        nil)

        (setq shr-color-visible-luminance-min 80))

      (use-package mu4e-alert
        :ensure t
        :defer t
        :config 
        (mu4e-alert-set-default-style 'libnotify)
        (add-hook 'after-init-hook #'mu4e-alert-enable-notifications)
        (add-hook 'after-init-hook #'mu4e-alert-enable-mode-line-display)
        )


    #+END_SRC

** Видео
*** mpsyt
*** yt-get
*** youtube-viever
*** streams Helm
*** streams
*** ....
** Показывать расписание транспорта [1/2]
*** [X] Электрички

    #+BEGIN_SRC 
        
    
    #+END_SRC

*** [ ] Маршруты
*** [ ] Такси

