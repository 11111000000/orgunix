function skewerMode(){
    var script=document.createElement('script');
    script.src='http://localhost:8080/skewer';
    document.body.appendChild(script);
}

function jOutlines () { return $('.outline-1').add('.outline-2').add('.outline-3').add('.outline-4').add('.outline-5').add('.outline-6').add('.outline-7') }  
function jOutlinesText () { return $('.outline-text-1').add('.outline-text-2').add('.outline-text-3').add('.outline-text-4').add('.outline-text-5').add('.outline-text-6').add('.outline-text-7') }
function jHeaders () { return $('h1').add('h2').add('h3').add('h4').add('h5').add('h6').add('h7') }

function diveIn (outline) {
  let pov = $('.pov')
  outline = $(outline)
  if(outline.is('.outline')) {
    let outlineParents = outline.parents(),
        index = outlineParents.index(pov)
    console.log(outlineParents,index)
    $('.pov').removeClass('pov')
    $('.pov-parent').removeClass('pov-parent')
    if(index > 2) {
      pov = outlineParents.eq(index - 2)
    } else  {
      pov = outline
    }
    pov.addClass('pov')
    pov.parents().addClass('pov-parent')
    // $('.pov-parent').removeClass('pov-parent')
    // outline.parents('.outline').addClass('pov-parent')
    // $('.pov').removeClass('pov')
    // outline.addClass('pov')
  }
}

function diveOut () {
  const current = $('.pov')
  const parent = current.parent('.pov-parent.outline')
  if(current.length > 0 && parent.length > 0) {
    parent.addClass('pov').removeClass('pov-parent')
    current.removeClass('pov')    
  }
}

(function () {    
  $(document).ready(function () {

    jOutlinesText().addClass('outline-text')

    jHeaders().addClass('header')

    jOutlines().addClass('outline')

    jOutlines().on('click', (event) => {
      event.stopPropagation()
      const closestOutline = $(event.target).closest('.outline');

      diveIn(closestOutline)
    })

    $('#content > .outline').first().addClass('pov')

    //skewerMode()

  })

  $(document).keyup(function(e) {    
    if (e.keyCode === 27) diveOut()   // esc
  })

})(window)
