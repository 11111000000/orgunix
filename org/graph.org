* Граф проекта
** заголовки index.org
 #+name: make-dot-index
 #+begin_src emacs-lisp  :results output :exports none
   (defun my/heading-pairs-from-file (filename)
     (with-current-buffer (find-file-noselect filename)
       (org-org-export-as-org)
       (org-element-map (org-element-parse-buffer 'headline )
           'headline
         (lambda(hl)
           (let ((parent (org-element-property :parent hl )))
             (and (eq (org-element-type parent) 'headline)
                  (list (org-element-property :title parent) (org-element-property :title hl))))))))
  (defun my/dot-from-list-of-pairs (table)
    (mapcar #'(lambda (x)
                  (princ (format "\"%s\" -> \"%s\";\n" (first x) (second x))))
            table))
   (my/dot-from-list-of-pairs (my/heading-pairs-from-file "./index.org"))
 #+end_src

** Граф index.org

 #+begin_src dot :file static/org-map-index.png :cmdline -Kfdp  -Tpng :var input=make-dot-index
   digraph {
     graph [rankdir=LR]
     splines=true
     pack=false
     node [shape=box]
     compound=true
     fontname="Iosevka"
     fontsize=10
     node [shape=box, style="filled,rounded", fillcolor="Floralwhite", fontname="Iosevka", fontsize=9, fontcolor="black"]
     $input
   }
 #+end_src

 #+RESULTS:
 [[file:static/org-map-index.png]]

