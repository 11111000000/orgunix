(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives
             '("org" . "http://orgmode.org/elpa/") t)
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))


(eval-when-compile
  (require 'use-package))

(package-install 'org)

(use-package htmlize :ensure t)

(require 'org)
(require 'ox-publish)

(setq org-publish-use-timestamps-flag nil
      user-full-name "Peter Kosov"
      user-mail-address "11111000000@email.com")

(let ((proj-base (file-name-directory default-directory)))
  (setq org-publish-project-alist
        `(("orgunix-tangle"
           :base-directory ,(concat proj-base "org")
           :recursive t
           :publishing-directory ,(concat proj-base  "public")
           :publishing-function org-babel-tangle-publish)
          ("orgunix-site"
           :base-directory ,(concat proj-base "org")
           :exclude ".*"
           :recursive nil
           :include ("index.org")
           :publishing-directory ,(concat proj-base  "public")
           :publishing-function org-html-publish-to-html)
          ("orgunix-static"
           :base-directory ,(concat proj-base "org/static")
           :base-extension any
           :publishing-directory ,(concat proj-base "public/static")
           :publishing-function org-publish-attachment)
          )))

(org-publish-all t)
