((nil . ((tab-width . 2)
         (js-indent-level . 2)))
 
 (css-mode . ((flycheck-stylelintrc . ".stylelintrc")))
 
 (html-mode . ((tab-width . 2))))
