#!/bin/sh

function tangle_files() {
    echo "Tangle config files with org-mode..."
    DIR=`pwd`/config/
    FILES=""

    for i in `ls ./config/ | grep \.org$`; do
        echo $i
        FILES="$FILES \"$i\""
    done

    emacs -Q --batch \
        --eval \
        "(progn
        (require 'org)(require 'ob)(require 'ob-tangle)
        (mapc (lambda (file)
        (find-file (expand-file-name file \"$DIR\"))
        (org-babel-tangle)
        (kill-buffer)) '($FILES)))"
    #2>&1 | grep Tangled
}

## mkdir -p ~/.emacs.d/init
## mkdir -p ~/.emacs.d/backup/
## mkdir -p ~/.emacs.d/tmp/
## rm -rf ~/.emacs.d/init/*

## tangle_files ## DO NOT RUN THIS

exit 0
